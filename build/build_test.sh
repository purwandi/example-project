#!/bin/bash

set -e
cd src/

echo "Checking Env"
node --version
npm --version

## STARTING SERVER
npm run start  > /dev/null 2>&1 &
APPS_PID=$!

npm run cypress:run

## KILLING SERVER
echo "Killing Server [$APPS_PID] ..."
kill -s TERM $APPS_PID

